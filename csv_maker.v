// pull all the http pages and parse

import net.http
import regex
import os
import json
import net.html

// returns list of course names
fn get_list_of_subjects(subject_list_page_url string) []string {
	// just yoink all the html on the page
	subject_list_page_html := http.get_text(subject_list_page_url)

	// Looking for link stubs that look like "/course-descriptions/csds/"
	regex_pattern_string := r'/course-descriptions/\w{4}/'
	mut re := regex.regex_opt(regex_pattern_string) or {
		panic(err)
	}
	subject_stub_list := re.find_all_str(subject_list_page_html)

	mut subject_list := []string{}

	for subject_stub in subject_stub_list {
		subject_code := subject_stub[21..25]
		subject_list << subject_code
	}

	mut filtered_subject_list := []string{}
	for subject in subject_list {
		if subject !in filtered_subject_list {
			filtered_subject_list << subject
		}
	}

	return filtered_subject_list
}

fn get_list_of_courses(base_url string, subject_code string) bool {
	stored_json := os.read_file('./JSONS/${subject_code}.json') or {
		''
	}

	if stored_json.len > 10 {
		return true
	}

	// variable to store all the courses' info that we find by parsing the HTML
	// TODO: make this not do the whole array to string thi
	mut subject_courses := map[string]map[string][]string{}

	subject_page_url := base_url + subject_code + '/'
	subject_page_html := http.get_text(subject_page_url)
	// subject_page_html := os.read_file("./CSDS.html") or { //Use this for testing :)
	// 	panic(err)
	// }

	// Replace all '&#160;' with ' '
	replacement_pattern_string := r'&#160;'
	mut reg := regex.regex_opt(replacement_pattern_string) or {
		panic(err)
	}
	updated_subject_page_html := reg.replace_simple(subject_page_html, ' ')

	// we're looking for HTML that looks like this:
	// <div class="courseblock">
	// <p class="courseblocktitle"><strong>AKKD 101.  Beginning Akkadian I.  3 Units.</strong></p>
	// <p class="courseblockdesc">
	// This course is the first of a sequence of two courses intended to cover the fundamentals of Akkadian grammar and a large number of the most common cuneiform signs encountered. A sample of texts (tablets) from the most important genres of cuneiform literature will be read. Counts for CAS Global &amp; Cultural Diversity Requirement.
	// <br/>
	// </p>
	// </div>
	mut doc := html.parse(updated_subject_page_html)
	tags := doc.get_tag('div')
	courseblock_tags := tags.filter(it.attributes['class'] == 'courseblock')

	for courseblock in courseblock_tags {
		text := courseblock.text()

		// Pull out just the course ID
		mut course_id := ''
		if text[8] == `.` {
			course_id = subject_code.to_upper() + text[5..8]
		} else {
			course_id = subject_code.to_upper() + text[5..9]
		}
		// Make sure we don't get one of them � characters in here
		if course_id[4] == 160 {
			course_id = course_id[..4] + course_id[5..]
		}

		// // pull out just the name of the course
		name_regex_string := r'\.  .*  \d'
		backup_name_regex_string := r'\.  .*  \.\d'
		mut regex_name := regex.regex_opt(name_regex_string) or {
			panic(err)
		}
		found_regex := regex_name.find_all_str(text)[0] or {
			regex_name = regex.regex_opt(backup_name_regex_string) or {
				panic(err)
			}
			regex_name.find_all_str(text)[0]
		}
		course_name := found_regex[3..(found_regex.len - 4)]

		// pull out just the units of the course
		course_units := found_regex[found_regex.len-1].ascii_str()

		// pull out the whole description of the course
		html_jumble := courseblock.str()
		parsed_html := html.parse(html_jumble)
		mut full_desc := parsed_html.get_tag('p').filter(it.attributes['class'] == 'courseblockdesc')[0].str()

		// pull out the other IDs that this course is listed under
		aka_from_desc_regex_string := r'Offered as.*\w{4} \d{3}.*text>\.'
		mut regex_full_aka := regex.regex_opt(aka_from_desc_regex_string) or {
			panic(err)
		}
		full_aka := regex_full_aka.find_all_str(full_desc)[0] or {
			''
		}
		full_aka_list := full_aka.split('\'')
		mut just_ids_aka := []string{}
		for i, element in full_aka_list {
			if i % 2 != 0 {
				shortened_id := element[..4] + element[5..]
				just_ids_aka << shortened_id
			}
		}
		just_ids_aka = just_ids_aka.filter(it != course_id)

		// pull out the pre/co-reqs
		prereqs_from_desc_reqex_string := r'Prereq.*\d{3}<text>\.'
		mut regex_full_prereqs := regex.regex_opt(prereqs_from_desc_reqex_string) or {
			panic(err)
		}
		full_prereqs := regex_full_prereqs.find_all_str(full_desc)[0] or {
			''
		}
		full_prereq_list := full_prereqs.split('\'')
		mut just_ids_reqs := []string{}
		for i, element in full_prereq_list {
			if i % 2 != 0 {
				shortened_id := element[..4] + element[5..]
				just_ids_reqs << shortened_id
			}
		}
		//Add this all of this course's info to the overall map of courses
		this_course_info := {
			'name': [course_name]
			'units': [course_units]
			'akas': just_ids_aka
			'reqs': just_ids_reqs
		}
		subject_courses[course_id] = this_course_info.clone()
	}

	// Now we're going to output all the info to a JSON file
	subject_courses_json := json.encode(subject_courses)

	os.create('./JSONS/${subject_code}.json') or {
		os.mkdir('./JSONS') or {
			panic("Error creating ./JSONS directory")
		}
		os.create('./JSONS/${subject_code}.json') or {
			panic("Couldn't create or find file ./JSONS/${subject_code}.json")
		}
	}

	os.write_file('./JSONS/${subject_code}.json', subject_courses_json) or {
		panic("Error writing JSON to file ./JSONS/${subject_code}.json")
	}

	return true
}







// change this if it ever changes in the future
base_url := "https://bulletin.case.edu/course-descriptions/"

list_of_course_codes := get_list_of_subjects(base_url)
// list_of_course_codes := ['bafi'] //Use this for testing :)

for code in list_of_course_codes {
	println(code)
	get_list_of_courses(base_url, code)
	println(code)
}