struct Test {
	id int

mut:
	list []Test
}

mut test1 := Test {
	id: 0
}

mut test2 := Test {
	id: 1
	list: [test1]
}

println(test2.list)