# CWRU Course Reqs (CCR)
- CCR is also [Creedence Clearwater Revival](https://www.youtube.com/watch?v=zUQiUFZ5RDw)
- This application helps to plan out course paths by creating a tree of requisistes for a given course

## Disclaimer:
Alright so the data that I'm trying to pull from the website is ALL OVER THE FRICKIN PLACE. This is the only part of data parsing I don't like, so I'm just gonna not because this is my party and I'll cry if I want to. Prereqs that are all required will be mixed with prereqs that are this OR that and yall are just gonna have to deal with it. Also if a Prereq is listed under an old subject ID (like EECS), there aren't links to those and those courses don't exist anymore so I'm not adding them to my tree. Also, prereqs like "graduate student" aren't included, I only care about class prereqs. 

If you don't like any of this, please call 719-266-2837 and press 4, thank you

## How to build / run:
1. Currently you must run `csv_maker.v` (a script which creates a JSON file, of course) before you run `ccr.v`
1. Make sure you have V installed: [vlang](https://vlang.io/)
1. cd to the directory where you have `csv_maker.v` and run `v run csv_maker.v`
1. once that finishes running, run `v run ccr.v [IDs]` from the same directory where `[IDs]` is a space-separated list of course IDs you want to find (example: `v run ccr.v CSDS233 MATH307 PHYS332`)
1. Copypaste the output from the command line to [this graph editor](https://csacademy.com/app/graph_editor/)
1. profit

## TODO:
- [ ] Actually finish the part where we use the data we've found and display it
- [ ] Make the regex that find the name and units able to get fractional units as well (.75, 1.5, etc)
- [ ] split pre and co-reqs
- [ ] actually be able to show which reqs are required and which are 'this OR that'
- [ ] Set up CI/CD
- [x] Use references to objects and play with those instead of deleting objects when we build the trees
  - you know, actually build a tree/DAG the proper way

## My ramblings:
Planned data structure:

- ID string
- aka string
- name string
- units int
- reqs array_of_pointers (includes co and prereqs because am lazy)

The idea is to made a linked graph of courses, each course is an object with a list of pre_reqs and a list of co_reqs, all of which are courses.

To traverse:

`pre_req_list(course) = pre_req_list(course.pre_req[0]) + pre_req_list(course.co_req[1]) ...`

fn add_new_course(ID, name, AKA, units, desc, reqIDs, coreqIDs) {
    // create the new struct w/ given info
    // for the reqs:
    // search all of the DAGs we've built up to look to see if the req that the new course requires exists somewhere
    //  if it exists, add a pointer to it in the new course struct
    //  if it doesn't exist yet, add a placeholder struct that just has the ID
    // search all the DAGs for any placeholder structs that are waiting on the new full struct for this course we just created
}


when we're adding a new course and we get to the req, we need to search all of the DAGs we've built up to look to see if the req that the new course requires exists somewhere. If it does, we add a pointer to it from the new course and call it a day