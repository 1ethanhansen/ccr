import x.json2
import os

// A struct to hold the information about each course
struct Course {
	id string [required]
mut:
	name string
	units string
	akas []string
	reqs []&Course
}

// Finds the parent node of the course we're searching for, allowing for editing the array of courses
fn iterative_search(search_course Course, id string, akas []string) &Course {
	// Make a stack to store all the courses we might check in the future
	mut course_stack := []&Course{}
	// Initially populate the stack with just the tree we're searching in
	course_stack << &search_course
	// while the stack isn't empty, we go through the stack looking for the parent of the course we're searching for
	for course_stack.len > 0 {
		current_course := course_stack.pop()
		if current_course.name == 'placeholder' || (current_course.reqs.len == 1 && current_course.reqs[0].id == 'FINA000') { // End of the line
			continue
		}
		// for each req in the current course, check if that req's id matches the given ID or the given AKAS...
		for req in current_course.reqs {
			if req.id == id || req.id in akas {
				return current_course
			}
			for aka in req.akas { // ...and check if the req's akas match the given ID or the given AKAS
				if id == aka || aka in akas {
					return current_course
				}
			}
		}
		// Didn't find anything this time around, so add all the reqs to the stack and restart
		course_stack << current_course.reqs
	}
	// Didn't find anything at all, return new end of the line course
	return &Course{id: 'FINA000'}
}

// For each course in the JSON, search to see if it already exists somewhere. If it doesn't, create a new one

// Get a list of all the JSON files stored in the JSON directory
list_of_files := os.ls('./JSONS') or {
	panic("Error running `ls ./JSONS`")
}
// list_of_files := ['math.json']  // Use this for testing :)

// Empty tree to store all the different courses is
end_of_the_line := Course{
	id: 'FINA000'
}
mut one_tree_to_rule_them_all := Course {
	id: 'ROOT000'
	name: 'One Tree To Rule Them All'
	reqs: [&end_of_the_line]
}

// For each file in the JSON, parse out all the course infos and add them to the trees where they belong
for file in list_of_files {
	// Read and decode the JSON
	json_string := os.read_file('./JSONS/$file') or {
		panic("Error reading ./JSONS/$file")
	}
	decoded := json2.raw_decode(json_string)?.as_map()

	// Go through all the course IDs
	for course_id, course_info in decoded {
		// Pull out the other info
		course_info_map := course_info.as_map()
		name_any := course_info_map['name'] or { panic("Can't get name for $course_id") }
		name := name_any.arr()[0].str()
		units_any := course_info_map['units'] or { panic("Can't get units for $course_id") }
		units := units_any.arr()[0].str()
		akas_any := course_info_map['akas'] or { panic("Can't get akas for $course_id") }
		akas := akas_any.arr()
		reqs_any := course_info_map['reqs'] or { panic("Can't get reqs for $course_id") }
		reqs := reqs_any.arr()

		mut akas_str := []string{}
		// akas are of type []Any from json2 so need to conver to []string
		for aka in akas {
			akas_str << aka.str()
		}

		// For each of the prereqs, search to see if it exists already to add it to this new struct
		mut req_structs := []&Course{}
		for req in reqs {
			reqstr := req.str() // Have to convert Any to string

			found_req_parent := iterative_search(one_tree_to_rule_them_all, reqstr, []) // Get the course that has the course we're looking for as a req
			
			// If the function returns an End of the Line course, we know to just add a new course and move on with our lives
			if found_req_parent.id == 'FINA000' {
				new_placeholder_req := Course {
					id: reqstr
					name: 'placeholder'
				}
				req_structs << &new_placeholder_req
			} else {  // If we got anything besides an end of the line, we have to check to make sure we aren't duplicating
				// First find the actual course we're looking for
				for found_req in found_req_parent.reqs {
					if found_req.id == reqstr || reqstr in found_req.akas {
						// Once found, we need to make sure we aren't duplicating anything
						mut all_existing_ids := req_structs.map(it.id)
						for existing_req in req_structs {
							all_existing_ids << existing_req.akas
						}
						// Make sure the found ID doesn't match any of the existing IDs or AKAS
						if found_req.id !in all_existing_ids {
							req_structs << found_req
							break
						}
					}
				}
			}
		}
		// println(req_structs)

		// if it doesn't have any reqs, make sure we add an empty req to mark it as the end of the line
		if req_structs.len == 0 {
			req_structs = [&end_of_the_line]
		}

		new_course := Course{
			id: course_id
			name: name
			units: units
			akas: akas_str
			reqs: req_structs
		}

		mut index := -10
		// go through all the unconnected trees and look for the course to see if we should replace a placeholder or start a new disconnected tree

		mut found_course_parent := iterative_search(one_tree_to_rule_them_all, course_id, akas_str)
		for i, found_req in found_course_parent.reqs {
			if (found_req.id == course_id || found_req.id in akas_str) && found_req.name == 'placeholder' {
				index = i
				break
			}
		}
		if index != -10 {
			found_course_parent.reqs[index] = &new_course
		} else {
			one_tree_to_rule_them_all.reqs << &new_course
		}
	}
}

// println(one_tree_to_rule_them_all)

looking_fors := os.args[1..]

for looking_for in looking_fors{
	found_tree := iterative_search(one_tree_to_rule_them_all, looking_for, [])
	for req in found_tree.reqs {
		if req.id == looking_for || req.id in found_tree.akas {
			mut course_stack := []&Course{}
			course_stack << req
			for course_stack.len > 0 {
				current_course := course_stack.pop()
				if current_course.reqs.len == 1 && current_course.reqs[0].id == 'FINA000' {
					continue
				}
				for next_req in current_course.reqs {
					println('$current_course.id $next_req.id')
				}
				course_stack << current_course.reqs
			}
		}
	}
	println("")
}


